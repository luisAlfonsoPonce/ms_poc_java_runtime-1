package mx.neoris.test.mongo.repository;

import mx.neoris.test.mongo.model.FacturaBean;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Copyright: Copyright (c) 2018 Company: Neoris.<br>
 * <p>
 * Descripción: Interface Repository (similar a Service) para aplicativo [<b>MicroServicios Spring Boot</b>].
 * Clase creada el 20180411.
 *
 * @version 1.0.0
 */
public interface BillRepository extends MongoRepository<FacturaBean, String> {
}
